This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to use the Apps
1. Registration Form, You can fill the form then submit it.
2. Line Chart, you can insert the number in URL with format chart?numbers=x,y;x,y . For example http://localhost:3000/chart?numbers=2,3;3,4;5,5


## `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

