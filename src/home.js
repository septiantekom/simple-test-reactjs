
import React, { Component } from 'react';
import './App.css';
import { Button, Form } from 'react-bootstrap';
class Home extends Component {
    state = {
        name: "",
        password: "",
        dateOfBirth: "",
        gender: "",
        email: ""
    }

    submit = (event) => {
        event.preventDefault();
        var request = new XMLHttpRequest();

        request.onreadystatechange = function (e) {
            if (request.readyState === 4 && request.status === 201) {
                alert('Data Successfully Submitted')
            }
        }
        request.open('POST', 'https://reqres.in/api/users', true);
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        request.send(JSON.stringify(this.state));
    }
    render() {
        return (

            <div className="App">
                <div className="App-header" >
                    <a className="App-title">Registration Form</a>
                    <Form onSubmit={this.submit}>
                        <Form.Control
                            placeholder="Name"
                            value={this.state.name}
                            onChange={e => this.setState({ name: e.target.value })}
                            required
                        />
                        <Form.Control
                            placeholder="Password"
                            type="password"
                            value={this.state.password}
                            onChange={e => this.setState({ password: e.target.value })}
                            required
                        />
                        <Form.Control
                            placeholder="Date of Birth"
                            type="date"
                            value={this.state.dateOfBirth}
                            onChange={e => this.setState({ dateOfBirth: e.target.value })}
                            required
                        />
                        <Form.Control
                            placeholder="Gender"
                            as="select"
                            value={this.state.gender}
                            onChange={e => this.setState({ gender: e.target.value })}
                            required
                        >
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </Form.Control>
                        <Form.Control
                            placeholder="Email"
                            type="email"
                            value={this.state.email}
                            onChange={e => this.setState({ email: e.target.value })}
                            required
                        />
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </div>

            </div>
        )
    }
}

export default Home;



