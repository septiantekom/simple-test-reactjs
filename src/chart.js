import React, { Component } from 'react';
import { Button, Form } from 'react-bootstrap';
import LineChart from 'react-linechart';
class Chart extends Component {
    state = {
        numbers:'',
        data : [
        ]

    }

    componentDidMount() {
        var href = window.location.href.split("?numbers=")
        this.setState({numbers:href[1]})
        var finalPoint = []
        if (href.length > 1 && href[1] !== 'undefined') {
            var x = href[1]
            var x = x.split(';')
            var points = []
            for (var i = 0; i < x.length; i++) {
                var axis = x[i].split(',')
                for (var ax = 0; ax < 2; ax++) {
                    if (ax === 0) {
                        var axisInside = {}
                        axisInside.x = parseInt(axis[0])
                    } else if (ax === 1) {
                        axisInside.y = parseInt(axis[1])
                        points.push(axisInside)
                    }
                }


            }
            finalPoint = {
                color: "steelblue",
                points: points
            }

            var newData =  this.state.data.concat(finalPoint)
            this.setState({data:newData});
        } else {
            this.setState({numbers:''});
            alert('Please fill number first and use this format in the field x,y;x,y, ex = 2,2;3,4;5,5')

        }


    }
    submit = (event) => {
        event.preventDefault();
        var href = window.location.href.split("?numbers=")
        window.location.href = href[0] + '?numbers=' + this.state.numbers
    }
    render() {
        return (
            <div>
                <div className="App-header" >
                    <a className="App-title">Chart Of Number</a>
                    <LineChart
                        width={600}
                        height={400}
                        data={this.state.data}
                    />
                    
                    <Form onSubmit={this.submit} style={{marginTop:30}}>
                        <Form.Group controlId="formBasicEmail">
                            {/* <Form.Label>List Of Number</Form.Label> */}
                            <Form.Control
                                placeholder="Enter Number"
                                value={this.state.numbers}
                                onChange={e => this.setState({ numbers: e.target.value })}
                            />
                        </Form.Group>
                        <Button variant="primary"  type="submit" >
                            Submit
  </Button>
                    </Form>
                </div>
            </div>
        );
    }
}

export default Chart;