import React,{  Component } from 'react';
import { Route,  BrowserRouter as Router, Switch, Link } from 'react-router-dom'
import Chart from './chart.js'
import Home from './home.js'
import './App.css';

class App extends Component {


  render() {
    return (
      <Router>
        <div className="App-home">
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/chart">Line Chart</Link>
          </li>
        </ul>

        <hr />
          <Switch>
          <Route path="/chart" component={Chart} />
            <Route path="/" component={Home} />

          </Switch>
        </div>
      </Router>

    );
  }
}

export default App;


